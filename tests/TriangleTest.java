import org.junit.Test;

import static org.junit.Assert.*;

public class TriangleTest {

    @Test
    public void checkDataTrue() {
        double[] arr = {3, 4, 5};
        boolean result = Triangle.checkData(arr);
        assertTrue(result);
    }
    @Test
    public void checkDataNegative() {
        double[] arr = {3, 4, -21};
        boolean result = Triangle.checkData(arr);
        assertFalse(result);
    }
    @Test
    public void checkDataFalseTriangle() {
        double[] arr = {3, 4, 10};
        boolean result = Triangle.checkData(arr);
        assertFalse(result);
    }

    @Test
    public void checkTriangleRectangular() {
        double[] arr = {3, 4, 5};
        String result = Triangle.definitionTriangle(arr);
        String expectation = "Треугольник прямоугольный";
        assertEquals(expectation,result);
    }
    @Test
    public void checkTriangleIsosceles() {
        double[] arr = {3, 3, 5};
        String result = Triangle.definitionTriangle(arr);
        String expectation = "Треугольник равнобедренный";
        assertEquals(expectation,result);
    }
    @Test
    public void checkTriangleRight() {
        double[] arr = {3, 3, 3};
        String result = Triangle.definitionTriangle(arr);
        String expectation = "Треугольник равносторонний";
        assertEquals(expectation,result);
    }
    @Test
    public void checkTriangle() {
        double[] arr = {1, 2, 3};
        String result = Triangle.definitionTriangle(arr);
        String expectation = "Треугольник не равнобедренный";
        assertEquals(expectation,result);
    }




}