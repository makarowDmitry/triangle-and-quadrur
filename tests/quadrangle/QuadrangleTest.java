package quadrangle;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class QuadrangleTest {

    @Test
    public void checkSquare(){
        ArrayList<Object> arrSide=new ArrayList<>();
        arrSide.add(2);
        arrSide.add(2);
        arrSide.add(2);
        arrSide.add(2);
        double cos = 0;
        String expectation = "Этот четырёхугольник является квадратом";
        String result = Quadrangle.checkSquareOrRhombus(arrSide,cos);
        assertEquals(expectation,result);
    }
    @Test
    public void checkRhombus() {
        ArrayList<Object> arrSide=new ArrayList<>();
        arrSide.add(2);
        arrSide.add(2);
        arrSide.add(2);
        arrSide.add(2);
        double cos = 1;
        String expectation = "Этот четырёхугольник является ромбом";
        String result = Quadrangle.checkSquareOrRhombus(arrSide,cos);
        assertEquals(expectation,result);
    }
    @Test
    public void NoCheckSquareOrRhombus() {
        ArrayList<Object> arrSide=new ArrayList<>();
        arrSide.add(2);
        arrSide.add(2);
        arrSide.add(3);
        arrSide.add(2);
        double cos = 1;
        String expectation = "Этот четырёхугольник не является ромбом или квадратом";
        String result = Quadrangle.checkSquareOrRhombus(arrSide,cos);
        assertEquals(expectation,result);
    }

    @Test
    public void checkParallelogram() {
        ArrayList<Object> arrSide=new ArrayList<>();
        arrSide.add(2);
        arrSide.add(2.24);
        arrSide.add(2);
        arrSide.add(2.24);
        double cos = 1;
        String expectation = "Этот четырёхугольник является параллелограмом";
        String result = Quadrangle.checkParallelogramOrRectangle(arrSide,cos);
        assertEquals(expectation,result);
    }
    @Test
    public void checkRectangle() {
        ArrayList<Object> arrSide=new ArrayList<>();
        arrSide.add(2);
        arrSide.add(2.24);
        arrSide.add(2);
        arrSide.add(2.24);
        double cos = 0;
        String expectation = "Этот четырёхугольник является прямоугольником";
        String result = Quadrangle.checkParallelogramOrRectangle(arrSide,cos);
        assertEquals(expectation,result);
    }
    @Test
    public void NoCheckParallelogramOrRectangle() {
        ArrayList<Object> arrSide=new ArrayList<>();
        arrSide.add(3);
        arrSide.add(2.24);
        arrSide.add(2);
        arrSide.add(2.24);
        double cos = 1;
        String expectation = "Этот четырёхугольник не является параллелограмом или прямоугольником";
        String result = Quadrangle.checkParallelogramOrRectangle(arrSide,cos);
        assertEquals(expectation,result);
    }
    @Test
    public void checkTrapeze() {
        ArrayList arrSide=new ArrayList<>();
        arrSide.add(5.0);
        arrSide.add(3.61);
        arrSide.add(2.0);
        arrSide.add(3.16);
        double [] arrPoints={1,4,6,4,4,7,2,7};
        String expectation = "Этот четырёхугольник является трапцией";
        String result = Quadrangle.checkTrapeze(arrPoints,arrSide);
        assertEquals(expectation,result);
    }

    @Test
    public void checkTrapezed() {
        ArrayList arrSide=new ArrayList<>();
        arrSide.add(3.0);
        arrSide.add(1.41);
        arrSide.add(2.0);
        arrSide.add(1.0);
        double [] arrPoints={1,1,4,1,3,2,1,2};
        String expectation = "Этот четырёхугольник является прямоугольной трапцией";
        String result = Quadrangle.checkTrapeze(arrPoints,arrSide);
        assertEquals(expectation,result);
    }

    @Test
    public void checkTrapezeIsosceles() {
        ArrayList arrSide=new ArrayList<>();
        arrSide.add(4.0);
        arrSide.add(2.24);
        arrSide.add(2.0);
        arrSide.add(2.24);
        double [] arrPoints={1,4,5,4,4,6,2,6};
        String expectation = "Этот четырёхугольник является равнобедренной трапцией";
        String result = Quadrangle.checkTrapeze(arrPoints,arrSide);
        assertEquals(expectation,result);
    }
    @Test
    public void NoCheckTrapeze() {
        ArrayList arrSide=new ArrayList<>();
        arrSide.add(5.0);
        arrSide.add(3.61);
        arrSide.add(7.0);
        arrSide.add(3.16);
        double [] arrPoints={1,4,6,4,32,7,2,7};
        String expectation = "Этот четырёхугольник не является трапцией";
        String result = Quadrangle.checkTrapeze(arrPoints,arrSide);
        assertEquals(expectation,result);
    }

}