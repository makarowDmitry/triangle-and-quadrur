import org.junit.Test;

import static org.junit.Assert.*;

public class QuadrurTest {

    @Test
    public void checkDataTrue() {
        double arr[] = {2, 4, 1};
        double dis = 8;
        boolean result = Quadrur.checkData(arr, dis);
        assertTrue(result);
    }

    @Test
    public void checkDataZero() {
        double arr[] = {2, 4, 0};
        double dis = 8;
        boolean result = Quadrur.checkData(arr, dis);
        assertFalse(result);
    }

    @Test
    public void checkDataNegativeDis() {
        double arr[] = {2, 4, 0};
        double dis = -8;
        boolean result = Quadrur.checkData(arr, dis);
        assertFalse(result);
    }

    @Test
    public void countDis() {
        double[] arrNum = {2, 5, 3};
        double dis = 1;
        assertTrue(dis == Quadrur.CountDis(arrNum));
    }

    @Test
    public void countX() {
        double[] arrNum = {2, 5, 3};
        double dis = 1;
        double x1 = -1;
        double x2 = -1.5;
        double[] expectation = {x1, x2};
        double[] result = Quadrur.CountX(arrNum, dis);
        assertTrue(expectation[0] == result[0] && expectation[1] == result[1]);
    }
}