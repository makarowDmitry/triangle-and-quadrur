import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class SizeShadowTest {
SizeShadow sizeShadow=new SizeShadow();
    @Test
    public void longShadow() {
        ArrayList<Double> arrayList =new ArrayList();
        arrayList.add(2.0);
        arrayList.add(6.0);
        arrayList.add(8.0);
        arrayList.add(12.0);
        double expectation = 8;
        double result = sizeShadow.LongShadow(arrayList);
        assertEquals(expectation, result, 0.0);
    }

    @Test
    public void deleteElementArr(){
        ArrayList<Double> arrayList =new ArrayList();
        arrayList.add(2.0);
        arrayList.add(6.0);
        arrayList.add(8.0);
        arrayList.add(12.0);
        arrayList.add(3.0);
        arrayList.add(4.0);
        ArrayList<Double> result = sizeShadow.deleteElementArr(arrayList);
        ArrayList<Double> expectation =new ArrayList();
        expectation.add(2.0);
        expectation.add(6.0);
        expectation.add(8.0);
        expectation.add(12.0);
        assertEquals(expectation,result);
    }
}