import java.util.InputMismatchException;
import java.util.Scanner;

class InputData {
    static double[] input() {
        Scanner scanner = new Scanner(System.in);
        double sideA = 0, sideB = 0, sideC = 0;
        try {
            System.out.println("Введите А: ");
            sideA = scanner.nextDouble();
            System.out.println("Введите В: ");
            sideB = scanner.nextDouble();
            System.out.println("Введите С: ");
            sideC = scanner.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Неверные данные");
        }
        return new double[]{sideA, sideB, sideC};
    }
}
