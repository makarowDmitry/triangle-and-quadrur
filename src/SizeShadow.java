import java.util.ArrayList;
import java.util.Scanner;

public class SizeShadow {
    boolean status = true;

    public static void main(String[] args) {
        ArrayList<Double> arrayList = new ArrayList<Double>();
        SizeShadow sizeShadow = new SizeShadow();;
        System.out.println(sizeShadow.LongShadow(sizeShadow.deleteElementArr( sizeShadow.input())));
    }

    public ArrayList input() {
        ArrayList<Double> arrayList = new ArrayList<Double>();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Количество отрезков:");
        int numberSegments = scanner.nextInt();
        for (int i = 0; i < numberSegments; i++) {
            System.out.println("Х1:");
            double x1 = scanner.nextDouble();
            arrayList.add(x1);
            status = true;
            while (status) {
                System.out.println("Х2:");
                double x2 = scanner.nextDouble();
                if (x1 < x2) {
                    arrayList.add(x2);
                    status = false;
                }
            }
        }
        return arrayList;
    }

    public ArrayList deleteElementArr(ArrayList arrayL) {
        ArrayList<Double> arrayList = arrayL;
        for (int i = 0; i < arrayList.size() - 1; i += 2) {
            for (int j = 2; j < arrayList.size() - 1; j += 2) {
                if (arrayList.get(i) < arrayList.get(j) && arrayList.get(i + 1) > arrayList.get(j + 1)) {
                    arrayList.remove(j+1);
                    arrayList.remove(j);

                }
            }
        }
        return arrayList;
    }
    public double LongShadow(ArrayList arrayL){
        ArrayList<Double> arrayList = arrayL;
        double longShad = 0;
        for (int i = 0; i < arrayList.size()-1; i += 2) {
            longShad += (arrayList.get(i + 1) - arrayList.get(i));
        }
        return longShad;
    }
}
