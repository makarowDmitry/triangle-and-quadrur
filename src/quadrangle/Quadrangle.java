package quadrangle;

import java.util.ArrayList;
import java.util.Scanner;

public class Quadrangle {


    public static void main(String[] args) {
        double arrPoints[] = input();
        while (true) {
            if (!checkData(arrPoints)) {
                System.out.println("Неверные данные");
                arrPoints = input();
            } else {
                break;
            }
        }
        ArrayList arrSide = side(arrPoints);
        double cos = cos(arrPoints);
        System.out.println(checkSquareOrRhombus(arrSide, cos));
        System.out.println(checkParallelogramOrRectangle(arrSide, cos));
        System.out.println(checkTrapeze(arrPoints, arrSide));

    }


    public static double[] input() {
        Scanner scanner = new Scanner(System.in);
        double aX = scanner.nextDouble();
        double aY = scanner.nextDouble();
        double bX = scanner.nextDouble();
        double bY = scanner.nextDouble();
        double cX = scanner.nextDouble();
        double cY = scanner.nextDouble();
        double dX = scanner.nextDouble();
        double dY = scanner.nextDouble();
        return new double[]{aX, aY, bX, bY, cX, cY, dX, dY};
    }

    public static boolean checkData(double arrPoints[]) {
        boolean check = true;
        for (double point : arrPoints) {
            if (point < 0) {
                check = false;
            }
        }
        return check;
    }

    public static double cos(double[] arrPoints) {
        return (arrPoints[0] * arrPoints[2] + arrPoints[1] * arrPoints[3]) /
                ((Math.sqrt(arrPoints[0] * arrPoints[0] + arrPoints[1] * arrPoints[1]) * (Math.sqrt(arrPoints[2] * arrPoints[2] + arrPoints[3] * arrPoints[3]))));
    }

    public static ArrayList side(double arrPoints[]) {
        ArrayList arrSide = new ArrayList();
        double side;
        for (int i = 0; i < arrPoints.length - 2; i += 2) {
            side = Math.sqrt(Math.pow(arrPoints[i + 2] - arrPoints[i], 2) + Math.pow(arrPoints[i + 3] - arrPoints[i + 1], 2));
            arrSide.add(side);
        }
        side = Math.sqrt(Math.pow(arrPoints[0] - arrPoints[6], 2) + Math.pow(arrPoints[1] - arrPoints[7], 2));
        arrSide.add(side);
        return arrSide;
    }


    public static String checkSquareOrRhombus(ArrayList arrSide, double cos) {
        boolean sideEqualuty = false;
        boolean rightAngle = false;

        if ((arrSide.get(0).equals(arrSide.get(1)) && arrSide.get(1).equals(arrSide.get(2)) && arrSide.get(2).equals(arrSide.get(3)))) {
            sideEqualuty = true;
        }
        if (cos == 0.0) {
            rightAngle = true;
        }
        if (sideEqualuty && rightAngle) {
            return ("Этот четырёхугольник является квадратом");

        }
        if (sideEqualuty) {
            return ("Этот четырёхугольник является ромбом");

        }
        return "Этот четырёхугольник не является ромбом или квадратом";
    }

    public static String checkParallelogramOrRectangle(ArrayList arrSide, double cos) {
        boolean parallelogram = false;
        if ((arrSide.get(0).equals(arrSide.get(2)) && (arrSide.get(1).equals(arrSide.get(3))))) {
            parallelogram = true;
        }
        if (cos == 0.0 && parallelogram) {
            return "Этот четырёхугольник является прямоугольником";
        } else if (parallelogram) {
            return "Этот четырёхугольник является параллелограмом";
        }
        return "Этот четырёхугольник не является параллелограмом или прямоугольником";
    }

    public static double midleSide(double arrPoints[]) {
        double midleXA = (arrPoints[0] + arrPoints[6]) / 2;
        double midleYA = (arrPoints[1] + arrPoints[7]) / 2;
        double midleXB = (arrPoints[2] + arrPoints[4]) / 2;
        double midleYB = (arrPoints[3] + arrPoints[5]) / 2;
        double sideMidle = Math.sqrt(Math.pow(midleXB - midleXA, 2) + Math.pow(midleYB - midleYA, 2));
        return sideMidle;
    }

    public static String checkTrapeze(double arrPoints[], ArrayList<Double> arrSide) {
        boolean trapeze = false;

        if (midleSide(arrPoints)==(arrSide.get(0)+arrSide.get(2))/2) {
            trapeze = true;
        }
        if (trapeze && arrSide.get(3).equals(arrSide.get(1))) {
            return "Этот четырёхугольник является равнобедренной трапцией";
        }
        if (trapeze && (arrPoints[0] == arrPoints[6] || arrPoints[2] == arrPoints[4])) {
            return "Этот четырёхугольник является прямоугольной трапцией";
        }
        if (trapeze) {
            return "Этот четырёхугольник является трапцией";
        }
        return "Этот четырёхугольник не является трапцией";
    }
}
