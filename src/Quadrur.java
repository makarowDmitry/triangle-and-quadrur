public class Quadrur {
    public static void main(String[] args) {
        conclusionData();
    }

    private static void conclusionData() {
        double[] arrNum = InputData.input();
        double dis = CountDis(arrNum);
        while (true) {
            if (!checkData(arrNum, dis)) {
                System.out.println("Неверные данные или Дискременант меньше нуля");
                arrNum = InputData.input();
                dis = CountDis(arrNum);
            } else {
                break;
            }
        }
        double[] arrX = CountX(arrNum, dis);
        for (int i = 0; i < 2; i++) {
            System.out.println("X" + (i + 1) + ": " + arrX[i]);
        }

    }

    public static boolean checkData(double[] arrNum, double dis) {
        boolean check = true;
        for (double number : arrNum) {
            if (number == 0) {
                check = false;
            }
        }
        if (dis < 0) {
            check = false;
        }
        return check;
    }

    public static double CountDis(double[] arrNum) {
        return Math.pow(arrNum[1], 2) - 4 * arrNum[0] * arrNum[2];
    }

    public static double[] CountX(double[] arrNum, double dis) {
        double x1 = (-arrNum[1] + Math.sqrt(dis)) / (2 * arrNum[0]);
        double x2 = (-arrNum[1] - Math.sqrt(dis)) / (2 * arrNum[0]);
        return new double[]{x1, x2};
    }
}

