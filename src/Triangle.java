public class Triangle {
    public static void main(String[] args) {
        definitionTriangle(input());
    }

    public static boolean checkData(double arrSide[]) {
        boolean check = true;
        for (double side : arrSide) {
            if (side < 0) {
                check = false;
            }
        }
        if (!((arrSide[0] + arrSide[1] > arrSide[2]) && (arrSide[1] + arrSide[2] > arrSide[0]) && (arrSide[0] + arrSide[2] > arrSide[1]))) {
            check = false;
        }
        return check;
    }

    public static double[] input() {
        double[] arrSide = InputData.input();
        while (true) {
            if (!checkData(arrSide)) {
                System.out.println("Неверные данные");
                arrSide = InputData.input();
            } else {
                break;
            }
        }
        return arrSide;
    }

    public static String definitionTriangle(double[] arrSide) {
        String result;

        if (arrSide[0] == arrSide[1] && arrSide[2] == arrSide[1]) {
            result = "Треугольник равносторонний";
        } else if (Math.pow(arrSide[0], 2) == Math.pow(arrSide[1], 2) + Math.pow(arrSide[2], 2) || Math.pow(arrSide[1], 2) == Math.pow(arrSide[0], 2) + Math.pow(arrSide[2], 2) || Math.pow(arrSide[2], 2) == Math.pow(arrSide[0], 2) + Math.pow(arrSide[1], 2)) {
            result = "Треугольник прямоугольный";
        } else if (arrSide[0] == arrSide[1] || arrSide[1] == arrSide[2] || arrSide[0] == arrSide[2]) {
            result = "Треугольник равнобедренный";
        } else {
            result = "Треугольник не равнобедренный";
        }
        return result;
    }
}
